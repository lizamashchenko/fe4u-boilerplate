const allUsers = [];
const courses = ['Mathematics', 'Physics', 'English', 'Computer Science',
  'Dancing', 'Chess', 'Biology', 'Chemistry', 'Law', 'Art', 'Medicine', 'Statistics'];
const Europe = ['Albania', 'Andorra', 'Armenia', 'Austria', 'Azerbaijan', 'Belarus', 'Belgium', 'Bosnia and Herzegovina',
  'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland', 'France', 'Georgia', 'Germany', 'Greece',
  'Hungary', 'Iceland', 'Ireland', 'Italy', 'Kazakhstan', 'Latvia', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Malta',
  'Moldova', 'Monaco', 'Montenegro', 'Netherlands', 'North Macedonia', 'Norway', 'Poland', 'Portugal', 'Romania', 'Russia',
  'San Marino', 'Serbia', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'Switzerland', 'Turkey', 'Ukraine', ' United Kingdom',
  'Vatican City', 'Åland', 'Channel Islands', 'Faroe Islands', 'Gibraltar', 'Guernsey', 'Isle of Man', 'Jersey', 'Kosovo',
  'Northern Cyprus', 'Northern Ireland', 'Svalbard'];
const Asia = ['Afghanistan', 'Armenia', 'Azerbaijan', 'Bahrain', 'Bangladesh', 'Bhutan', 'British Indian Ocean Territory',
  'Brunei', 'Cambodia', 'China', 'Cyprus', 'Egypt', 'Georgia', 'India', 'Indonesia', 'Iran', 'Iraq', 'Israel', 'Japan',
  'Jordan', 'Kazakhstan', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Lebanon', 'Malaysia', 'Maldives', 'Mongolia', 'Myanmar', 'Nepal',
  'North Korea', 'Oman', 'Pakistan', 'Palestine', 'Philippines', 'Qatar', 'Russia', 'Saudi Arabia', 'Singapore', 'South Korea',
  'Sri Lanka', 'Syria', 'Taiwan', 'Tajikistan', 'Thailand', 'East Timor', 'Turkmenistan', ' United Arab Emirates', 'Uzbekistan',
  'Vietnam', 'Yemen'];
const Oceania = ['Australia', 'Fiji', 'Kiribati', 'Marshall Islands', 'Micronesia', 'Nauru', 'New Zealand', 'Palau',
  'Papua New Guinea', 'Samoa', 'Solomon Islands', 'Tonga', 'Tuvalu', 'Vanuatu', 'American Samoa', 'Ashmore and Cartier Islands',
  'Baker Island', 'Cook Islands', 'Coral Sea Islands', 'Easter Island', 'French Polynesia', 'Galapagos Islands', 'Guam',
  'Howland Island', 'Jarvis Island', 'Johnston Atoll', 'Kingman Reef', 'Midway Atoll', 'New Caledonia', 'Niue', 'Norfolk Island',
  'Northern Mariana Islands', 'Palmyra Atoll', 'Papua', 'Pitcairn Islands', 'San Andrés and Providencia', 'Tokelau',
  'Wake Island', 'Wallis and Futuna', 'West Papua'];
const Africa = ['Algeria', 'Angola', 'Benin', 'Botswana', 'Burkina Faso', 'Burundi', 'Cape Verde', 'Cameroon',
  'Central African Republic', 'Chad', 'Comoros', 'Congo', 'Democratic Republic of the Congo', 'Djibouti', 'Egypt',
  'Equatorial Guinea', 'Eritrea', 'Eswatini', 'Ethiopia', 'Gabon', 'Gambia', 'Ghana', 'Guinea', 'Guinea-Bissau',
  'Republic of Côte d\'Ivoire', 'Kenya', 'Lesotho', 'Liberia', 'Libya', 'Madagascar', 'Malawi', 'Mali', 'Mauritania',
  'Mauritius', 'Morocco', 'Mozambique', 'Namibia', 'Niger', 'Nigeria', 'Rwanda', 'Sao Tome and Principe', 'Senegal',
  'Seychelles', 'Sierra Leone', 'Somalia', 'South Africa', 'South Sudan', 'Sudan', 'Tanzania', 'Togo', 'Tunisia',
  'Uganda', 'Zambia', 'Zimbabwe', 'Canary Islands', 'Ceuta', 'French Southern and Antarctic Lands', 'Madeira', 'Mayotte',
  'Melilla', 'Pelagie Islands', 'Plazas de Soberania', 'Reunion', 'Saint Helena', 'Ascension and Tristan da Cunha', 'Socotra Archipelago'];
const Americas = ['Antigua and Barbuda', 'Bahamas', 'Barbados', 'Belize', 'Canada', 'Costa Rica', 'Cuba', 'Dominica',
  'Dominican Republic', 'El Salvador', 'Grenada', 'Guatemala', 'Haiti', 'Honduras', 'Jamaica', 'Mexico', 'Nicaragua',
  'Panama', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Vincent and the Grenadines', 'Trinidad and Tobago', 'United States',
  'Anguilla', 'Aruba', 'Bermuda', 'Bonaire', 'British Virgin Islands', 'Cayman Islands', 'Clipperton Island', 'Curaçao',
  'Greenland', 'Guadeloupe', 'Martinique', 'Montserrat', 'Navassa Island', 'Puerto Rico', 'Saba', 'Saint Barthélemy',
  'Saint Martin', 'Saint Pierre and Miquelon', 'Sint Eustatius', 'Sint Maarten', 'Turks and Caicos', 'US Virgin Islands',
  'Argentina', 'Bolivia', 'Brazil', 'Chile', 'Colombia', 'Ecuador', 'Guyana', 'Paraguay', 'Peru', 'Suriname', 'Uruguay',
  'Venezuela', 'Bouvet Island', 'Falkland Islands', 'French Guinea', 'Nueva Esparta', 'South Georgia and the South Sandwich Islands'];
let filteredUsers;
let sortedUsers;
const favouriteUsers = [];
const elementsInSlider = 5;
let sliderCounter = 0;

const ageFilter = document.getElementById('age');
const regionFilter = document.getElementById('region');
const sexFilter = document.getElementById('sex');
const photoFilter = document.getElementById('with-photo');
const favouriteFilter = document.getElementById('only-favorites');
const table = document.getElementById('statistics-table');
const tableName = document.querySelector('.header-name');
const tableAge = document.querySelector('.header-age');
const tableGender = document.querySelector('.header-gender');
const tableCountry = document.querySelector('.header-country');
const tableSpeciality = document.querySelector('.header-speciality');
const topTeacherMain = document.querySelector('.teachers-grid');
const favoritesMain = document.querySelector('.teachers-view');
const searchButton = document.querySelector('.search-button');
const pagingDiv = document.querySelector('.paging');

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}
function changeUser(user) {
  const courseName = courses[getRandomInt(courses.length)];
  return {
    gender: user.gender,
    title: user.name.title,
    full_name: `${user.name.last} ${user.name.first}`,
    city: user.location.city,
    state: user.location.state,
    country: user.location.country,
    postcode: user.location.postcode,
    coordinates: user.location.coordinates,
    timezone: user.location.timezone,
    email: user.email,
    b_date: user.dob.date,
    age: user.dob.age,
    phone: user.phone,
    picture_large: user.picture.large,
    picture_thumbnail: user.picture.thumbnail,
    id: user.id,
    favorite: user.favorite,
    course: courseName,
    bg_color: user.bg_color,
    note: user.note,
  };
}
function validateUser(user) {
  const nameValid = typeof user.full_name === 'string'
      && user.full_name[0].toUpperCase() === user.full_name[0];
  const genderValid = typeof user.gender === 'string' || user.gender == null;
  const stateValid = (typeof user.state === 'string'
          && user.state[0].toUpperCase() === user.state[0])
      || user.state == null;
  const noteValid = typeof user.note === 'string' || user.note == null;
  const cityValid = (typeof user.city === 'string'
      && user.city[0].toUpperCase() === user.city[0]) || user.city == null;
  const ageValid = !Number.isNaN(user.age) || user.age == null;
  return nameValid && genderValid
      && stateValid && noteValid
      && cityValid && ageValid;
}
function filterUsers(users, region, age, gender, favorite, photo) {
  let filtered = [...users];
  if (region != null && region !== 'all') {
    filtered = filtered.filter((entry) => {
      if (region === 'Europe') {
        return Europe.includes(entry.country);
      }
      if (region === 'Asia') {
        return Asia.includes(entry.country);
      }
      if (region === 'Americas') {
        return Americas.includes(entry.country);
      }
      if (region === 'Oceania') {
        return Oceania.includes(entry.country);
      }
      return Africa.includes(entry.country);
    });
  }
  if (age != null && age !== 'all') {
    let min; let max;
    if (age === '18-35') {
      min = 18;
      max = 35;
    } else if (age === '32-51') {
      min = 32;
      max = 51;
    } else if (age === '52-81') {
      min = 52;
      max = 81;
    } else {
      min = 82;
      max = 2000;
    }
    filtered = filtered.filter((entry) => entry.age >= min && entry.age <= max);
  }
  if (gender != null && gender !== 'all') {
    filtered = filtered.filter((entry) => entry.gender === gender);
  }
  if (favorite != null) {
    filtered = filtered.filter((entry) => {
      if (favorite === true) return entry.favorite === favorite;
      return true;
    });
  }
  if (photo != null) {
    filtered = filtered.filter((entry) => {
      if (photo === true) return (entry.picture_thumbnail != null) === photo;
      return true;
    });
  }
  return filtered;
}
function sortUsers(users, field, direction) {
  users.sort((a, b) => {
    if (direction === 'asc') {
      if (a[field] < b[field]) return -1;
      if (a[field] > b[field]) return 1;
      return 0;
    }
    if (direction === 'dsc') {
      if (a[field] > b[field]) return -1;
      if (a[field] < b[field]) return 1;
      return 0;
    }
    return 0;
  });
  return users;
}
function searchUser(users, value) {
  return users.filter((entry) => entry.full_name === value
      || entry.note === value
      || entry.age === parseInt(value, 10));
}
function findByAge(users, value, comparator) {
  return users.filter((entry) => {
    if (comparator === '>') {
      return entry.age > value;
    }
    if (comparator === '<') {
      return entry.age < value;
    }
    if (comparator === '=') {
      return entry.age === value;
    }
    return 0;
  });
}
function calculateAge(birthdate) {
  const today = new Date();
  const birthDate = new Date(birthdate);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age -= 1;
  }
  return age;
}
function createTeacherProfile(user, parentNode) {
  const teacherProfile = document.createElement('div');
  teacherProfile.className = 'teacher-view';
  teacherProfile.id = user.full_name;
  const picture = document.createElement('div');
  picture.className = 'user-profile';
  const anchor = document.createElement('a');
  anchor.href = '#full-teacher-view';
  anchor.setAttribute('onclick', 'loadUser(this.parentElement.nextSibling.innerText)');
  const pfp = document.createElement('img');
  pfp.className = 'user-photo';
  pfp.src = user.picture_thumbnail;
  pfp.alt = 'Teacher\'s profile picture';
  const name = document.createElement('h3');
  name.className = 'teacher-name';
  name.innerText = user.full_name;
  const speciality = document.createElement('h5');
  speciality.className = 'teacher-speciality';
  speciality.innerText = user.course;
  const country = document.createElement('h6');
  country.className = 'teacher-country';
  country.innerText = user.country;
  anchor.appendChild(pfp);
  picture.appendChild(anchor);
  teacherProfile.appendChild(picture);
  teacherProfile.appendChild(name);
  teacherProfile.appendChild(speciality);
  teacherProfile.appendChild(country);
  parentNode.appendChild(teacherProfile);
}
function updateFavourites(slide) {
  if (favouriteUsers.length <= elementsInSlider) {
    favoritesMain.innerHTML = '';
    favouriteUsers.forEach((element) => createTeacherProfile(element, favoritesMain));
    sliderCounter = 0;
    return true;
  }
  if (slide !== undefined) {
    const attemptSliderCounter = sliderCounter + parseInt(slide, 10);
    if (attemptSliderCounter + elementsInSlider > favouriteUsers.length
        || attemptSliderCounter < 0) {
      return true;
    }
    sliderCounter += parseInt(slide, 10);
    const slider = document.querySelector('.teachers-view');
    while (slider.firstChild) {
      slider.removeChild(slider.firstChild);
    }
    for (let i = 0; i < elementsInSlider; i += 1) {
      createTeacherProfile(favouriteUsers[sliderCounter + i], favoritesMain);
    }
  }
  return true;
}
function removeTeacherViews() {
  const teacherViews = document.querySelector('.full-teacher-view');
  while (teacherViews.firstChild) {
    teacherViews.removeChild(teacherViews.firstChild);
  }
  return true;
}
function cleanTopTeachers() {
  while (topTeacherMain.firstChild) {
    topTeacherMain.removeChild(topTeacherMain.firstChild);
  }
  return true;
}
function removeStatistics() {
  const statisticRows = document.querySelectorAll('.table-row');
  statisticRows.forEach((element) => element.remove());
}
function addAStar(user) {
  const userProfile = document.getElementById(user.full_name).firstElementChild;
  console.log(userProfile);
  const starWrap = document.createElement('div');
  starWrap.className = 'favorite-star';
  const star = document.createElement('img');
  star.src = 'images/star.png';
  star.alt = 'is favorite';
  starWrap.append(star);
  userProfile.prepend(starWrap);
}
function addToFavorites(userNameTag) {
  let counter = 0;
  while (userNameTag[counter] !== ':') {
    counter += 1;
  }
  counter += 2;
  let userName = '';
  for (let i = counter; i < userNameTag.length; i += 1) {
    userName += userNameTag[i];
  }
  const user = searchUser(allUsers, userName)[0];
  user.favorite = true;
  favouriteUsers.push(user);
  addAStar(user);
  removeTeacherViews();
  updateFavourites();
  return true;
}
function createView(user) {
  const teacherViewDiv = document.querySelector('.full-teacher-view');
  const name = document.createElement('h1');
  name.innerText = `Full name: ${user.full_name}`;
  const sex = document.createElement('p');
  sex.innerText = `Sex: ${user.gender}`;
  const age = document.createElement('p');
  age.innerText = `Age: ${user.age}`;
  const email = document.createElement('p');
  email.innerText = `Email: ${user.email}`;
  const phoneNumber = document.createElement('p');
  phoneNumber.innerText = `Phone number: ${user.phone}`;
  const country = document.createElement('p');
  country.innerText = `Country: ${user.country}`;
  const city = document.createElement('p');
  city.innerText = `City: ${user.city}`;
  const comment = document.createElement('p');
  comment.innerText = `Comment: ${user.comment}`;
  const map = document.createElement('iframe');
  map.width = '600';
  map.height = '500';
  map.id = 'gmap_canvas';
  map.src = 'https://maps.google.com/maps?q=Manhattan,%20NY%2010036,%20United%20States&t=&z=13&ie=UTF8&iwloc=&output=embed';
  const color = document.createElement('p');
  color.innerText = 'Colors:';
  const colorList = document.createElement('ul');
  const black = document.createElement('li');
  black.data_color = 'black';
  black.innerText = 'black';
  const green = document.createElement('li');
  green.data_color = 'green';
  green.innerText = 'green';
  const red = document.createElement('li');
  red.data_color = 'red';
  red.innerText = 'red';
  const blue = document.createElement('li');
  blue.data_color = 'blue';
  blue.innerText = 'blue';
  const controls = document.createElement('div');
  controls.className = 'controls';
  const anchorCancel = document.createElement('a');
  anchorCancel.href = '#header';
  anchorCancel.innerText = 'Close';
  const anchorFav = document.createElement('a');
  anchorFav.href = '#header';
  anchorFav.innerText = 'Add to favorites';
  controls.append(anchorFav, anchorCancel);
  colorList.append(black, green, red, blue);
  // eslint-disable-next-line max-len
  teacherViewDiv.append(name, sex, age, email, phoneNumber, country, city, comment, map, color, colorList, controls);
  anchorFav.setAttribute('onclick', 'addToFavorites(this.parentNode.parentNode.firstElementChild.innerText)');
  anchorCancel.setAttribute('onclick', 'removeTeacherViews()');
}
function loadUser(userName) {
  const user = searchUser(allUsers, userName)[0];
  createView(user);
}
function updateTopTeachers() {
  document.querySelector('.teachers-grid').innerHTML = '';
  filteredUsers.forEach((element) => createTeacherProfile(element, topTeacherMain));
}
function addToTable(user) {
  const tableRow = document.createElement('tr');
  tableRow.className = 'table-row';
  const nameTd = document.createElement('td');
  nameTd.innerText = user.full_name;
  const courseTd = document.createElement('td');
  courseTd.innerText = user.course;
  const ageTd = document.createElement('td');
  ageTd.innerText = user.age;
  const genderTd = document.createElement('td');
  genderTd.innerText = user.gender;
  const countryTd = document.createElement('td');
  countryTd.innerText = user.country;
  tableRow.append(nameTd, courseTd, ageTd, genderTd, countryTd);
  table.append(tableRow);
}
function filterChange() {
  filteredUsers = filterUsers(allUsers, regionFilter.value, ageFilter.value,
    sexFilter.value, favouriteFilter.checked, photoFilter.checked);
  updateTopTeachers();
}
function getOrder(users, field) {
  let counter = 0;
  while (users[counter][field] === users[counter + 1][field]) {
    counter += 1;
  }
  return users[counter][field] < users[counter + 1][field] ? 'dsc' : 'asc';
}
function sortTable(field) {
  const order = getOrder(sortedUsers, field);
  sortedUsers = sortUsers(sortedUsers, field, order);
  const tableElements = document.querySelectorAll('.table-row');
  tableElements.forEach((element) => {
    element.remove();
  });
  sortedUsers.forEach(addToTable);
  return true;
}
function searchButtonF() {
  const foundUsers = searchUser(allUsers, document.getElementById('search-field').value);
  foundUsers.forEach(createView);
  return true;
}
function createNewTeacher() {
  const nameField = document.getElementById('name');
  const birthdayField = document.getElementById('birthdate');
  const emailField = document.getElementById('email');
  const phoneField = document.getElementById('phone-number');
  const backgroundField = document.getElementById('background');
  const countryField = document.getElementById('country');
  const cityField = document.getElementById('city');
  const commentField = document.getElementById('comment');
  const newUser = {
    gender: document.querySelector('input[name="gender"]:checked').id,
    title: undefined,
    full_name: nameField.value,
    city: cityField.value,
    state: undefined,
    country: countryField.value,
    postcode: undefined,
    coordinates: undefined,
    timezone: undefined,
    email: emailField.value,
    b_date: birthdayField.value,
    age: calculateAge(birthdayField.value),
    phone: phoneField.value,
    picture_large: undefined,
    picture_thumbnail: undefined,
    id: undefined,
    favorite: false,
    course: courses[getRandomInt(courses.length)],
    bd_color: backgroundField.value,
    note: commentField.value,
  };
  allUsers.push(newUser);

  fetch('http://localhost:3000/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(newUser),
  }).then((response) => {
    response.json();
  })
    .then((data) => console.log(data))
    .catch((error) => {
      console.log(error);
    });
  return true;
}
async function addMore() {
  await fetch('https://randomuser.me/api/?results=10')
    .then((response) => response.json())
    .then((data) => data.results.forEach((user) => {
      allUsers.push(changeUser(user));
      cleanTopTeachers();
      removeStatistics();
    }))
    .catch((error) => console.log('Fetch error -> ', error));
}
function changePage(value) {
  while (pagingDiv.firstChild) {
    pagingDiv.removeChild(pagingDiv.firstChild);
  }
  console.log(`value ${value}`);
  console.log(allUsers.length / 10);
  const first = document.createElement('p');
  first.innerText = 'First';
  first.className = 'page';
  first.id = '1';
  const current = document.createElement('p');
  const next = document.createElement('p');
  if (value === 'last') {
    console.log('caught last');
    next.innerText = 'Last';
    next.className = 'page';
    next.id = value;
    current.innerText = allUsers.length / 10 - 1;
    current.className = 'page';
    current.id = allUsers.length / 10 - 1;
  } else {
    next.innerText = parseInt(value, 10) + 1;
    next.className = 'page';
    next.id = parseInt(value, 10) + 1;
    current.innerText = value;
    current.className = 'page';
    current.id = value;
  }
  const last = document.createElement('p');
  last.innerText = 'Last';
  last.className = 'page';
  last.id = 'last';
  const divider1 = document.createElement('p');
  divider1.innerText = '...';
  divider1.className = 'page';
  const divider2 = document.createElement('p');
  divider2.innerText = '...';
  divider2.className = 'page';
  pagingDiv.appendChild(first);
  if (value !== '1' && value !== '2') {
    console.log('caught not 1 2 adding divider');
    pagingDiv.appendChild(divider1);
  }
  first.setAttribute('onclick', 'changePage(this.id)');
  current.setAttribute('onclick', 'changePage(this.id)');
  next.setAttribute('onclick', 'changePage(this.id)');
  last.setAttribute('onclick', 'changePage(this.id)');
  if (value !== '1') {
    console.log('caught not 1 adding current');
    pagingDiv.appendChild(current);
  }
  if (parseInt(value, 10) !== allUsers.length / 10 - 1 && value !== 'last') {
    console.log('caught not pre-last adding next and divider 2');
    pagingDiv.append(next, divider2);
  }
  const add = document.createElement('p');
  add.innerText = 'Load more';
  add.className = 'add-more';
  add.setAttribute('onclick', 'addMore()');
  pagingDiv.append(last, add);

  cleanTopTeachers();
  removeStatistics();
  if (value === 'last') {
    // eslint-disable-next-line no-param-reassign
    value = allUsers.length / 10;
  }
  for (let i = (value - 1) * 10; i < value * 10; i += 1) {
    addToTable(allUsers[i]);
    createTeacherProfile(allUsers[i], topTeacherMain);
  }
}

async function getUsers() {
  await fetch('https://randomuser.me/api/?results=50')
    .then((response) => response.json())
    .then((data) => {
      data.results.forEach((user, index) => {
        const correctUser = changeUser(user);
        allUsers.push(correctUser);
        if (index < 10) {
          createTeacherProfile(correctUser, topTeacherMain);
          addToTable(correctUser);
        }
      });
    })
    .catch((error) => console.log('Fetch error -> ', error));
}
getUsers();
sortedUsers = [...allUsers];

ageFilter.onchange = filterChange;
regionFilter.onchange = filterChange;
sexFilter.onchange = filterChange;
photoFilter.onchange = filterChange;
favouriteFilter.onchange = filterChange;
tableName.setAttribute('onclick', 'sortTable(\'full_name\')');
tableAge.setAttribute('onclick', 'sortTable(\'age\')');
tableGender.setAttribute('onclick', 'sortTable(\'gender\')');
tableCountry.setAttribute('onclick', 'sortTable(\'country\')');
tableSpeciality.setAttribute('onclick', 'sortTable(\'course\')');
searchButton.setAttribute('onclick', 'searchButtonF()');
const pages = document.querySelectorAll('.page');
pages.forEach((page) => page.setAttribute('onclick', 'changePage(this.id)'));
document.querySelector('.add-more').setAttribute('onclick', 'addMore()');
